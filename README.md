# Lab 1


Abdullah Mohammed Bajaman


1936299



1.	We change the “private int quantity;” TO “private static int quantity;”

==	By this change we ensure that we made this attribute a class variable not an instance variable. Moreover, we also change the method signature to be a static method to be accessed by all classes.


2.	….



3.   -   “Product p4 = new FoodProduct(3452, 10.0, "Cheddar Cheese",
        LocalDate.parse("2022-06-07"));
        Product p5 = new ElectricProduct(4875, 30.0, "Extension cord", "220v");”
     
-	public abstract void getSellableStatus();
-	abstract class Product {

==	By this we change the class Product to be an abstract class. Therefore, it will not allow us to instantiate from it.



4.     Product p1 = new FoodProduct(6745, 5.50, "Penne Pasta");
         Product p2 = new FoodProduct(8853, 6.50, "Spaghetti Pasta");
         Product p3 = new FoodProduct(2106, 4.50, "Linguine Pasta");
         p3.printTotalQuantity();

==	We may instantiate objects from Product of FoodProduct type to apply Polymorphism.


5. We change the method signature to be FINAL. Therefore, subclasses will not be able to change this method.


6.  Create a new class called Order, and extend to Product class so we can link them together without mixing. 


7. we make the attribute private, then we put an -If statement- to check if the weight is below 0 or not. By this, we make sure other classes can not put invalid input.
